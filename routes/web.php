<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ServiceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', function () {
    return view('backend.dashboard');
});


// routes for category

Route::prefix('admin')->group(function () {
    Route::get('/category', [CategoryController::class, 'create'])->name('category.create');
    Route::post('/category/store', [CategoryController::class, 'store'])->name('category.store');
    
    Route::get('/category/index', [CategoryController::class, 'index'])->name('category.index');
    Route::get('/category/edit/{id}', [CategoryController::class, 'edit'])->name('category.edit');
    Route::delete('/category/{category}/destroy', [CategoryController::class, 'destroy'])->name('category.destroy');
    Route::post('/category/{category}/update', [CategoryController::class, 'update'])->name('category.update');
    Route::get('/deleted-categories', [CategoryController::class, 'trash'])->name('category.trash');
    
    
    Route::get('/deleted-categories/{id}/restore', [CategoryController::class, 'restore'])->name('category.restore');
    Route::delete('/deleted-categories/{id}', [CategoryController::class, 'delete'])->name('category.delete');
    
    
    });


    
    // routes for service
    Route::prefix('admin')->group(function () {

        Route::get('/service/create', [ServiceController::class, 'create'])->name('service.create');



    });