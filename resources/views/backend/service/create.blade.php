



<x-backend.layouts.master>

{{-- add category --}}
<form method="POST" action="" enctype="multipart/form-data">

@csrf
<div class="mb-3">
<label for="name" class="form-label">Name</label>
<input name="name" type="text" class="form-control" id="name" value="">                   

<br>
<div class="mb-3">
<label for="name" class="form-label">Category Name</label>
<input name="category_name" type="text" class="form-control"  value=""> 

<br>
<div class="mb-3">
<label for="brand_name" class="form-label">Brand Name</label>
<input name="brand_name" type="text" class="form-control"  value=""> 


<br>
<div class="mb-3">
<label for="description" class="form-label">description</label>
<input name="description" type="text" class="form-control"  value=""> 

<br>
<input type="file" name="image">


<br>
<div class="mb-3">
<label for="status" class="form-label">status</label>
<input name="status" type="text" class="form-control"  value=""> 

<button type="submit" class="btn btn-primary">Add</button>

</form>

</x-backend.layouts.master>