<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
   //   public function create(){
   //      //  dd('create page');

   //       return view('category.create');
   //   }

     public function store(Request $request){


        $categorydata=$request->all();
        // dd($categorydata);

        Category::create($categorydata);

        return Redirect()->route('category.index');

        // $product=Product::create($productdata);



     }
     public function index(){

        $categorydata=Category::orderBy('id','desc')->get();

        //  dd($categorydata);

        return view('category.index', compact('categorydata'));


     }
     public function edit($id){

      $categories = Category::findOrFail($id);

      // $categories = Category::pluck('category_title', 'id')->toArray();
      // dd($categories);
      return view('category.edit', compact('categories'));
      // dd($categories);
     }


      public function update(Category $category){

         try
         {
          $categorydata=request()->all();
          $category->update($categorydata);
          return Redirect()->route('category.index');
         }
       catch (QueryException $e) 
         {
           return redirect()->back()->withInput()->withErrors($e->getMessage());
         }

      }
      public function destroy(Category $category){

         $category->delete();
         // dd($category);
         return Redirect()->route('category.index');

      }

      public function trash()
    {

        $categories = Category::onlyTrashed()->get();

      //   dd($categories);
       return view('category.trash', compact('categories'));
    }

    public function restore($id){
      Category::withTrashed()
      ->where('id', $id)
      ->restore();

      return redirect()->route('category.trash')->withMessage('Successfully Restored !');
    }

    public function delete($id)
    {
      Category::withTrashed()
            ->where('id', $id)
            ->forceDelete();

        return redirect()->route('Category.trash')->withMessage('Deleted Successfully');
    }





}
